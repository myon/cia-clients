# Copyright (C) 2006-2007 Jelmer Vernooij <jelmer@samba.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""Blackbox tests."""

import os

import bzrlib
from bzrlib.tests import TestCaseWithTransport


class TestBranch(TestCaseWithTransport):
    def test_get_cia_project(self):
        os.mkdir("bla")
        os.chdir("bla")
        self.run_bzr('init')
        self.assertEquals("None\n", self.run_bzr('cia-project')[0])

    def test_set_cia_project(self):
        os.mkdir("bla")
        os.chdir("bla")
        self.run_bzr('init')
        self.assertEquals("", self.run_bzr('cia-project myproject')[0])
        self.assertEquals("myproject\n", self.run_bzr('cia-project')[0])

    def test_submit_dry_run(self):
        tree = self.make_branch_and_tree("bla")
        revid = tree.commit("bla", timestamp=1180551778, 
                committer="Jelmer Vernooij <jelmer@ganieda.lan.vernstok.nl>")

        os.chdir("bla")
        self.run_bzr('cia-project myproject')

        self.assertEqualDiff("""
<message>
  <generator> 
    <name>bzr</name> 
    <version>%s</version> 
    <url>http://samba.org/~jelmer/bzr/cia_bzr.py</url>
  </generator>
  <source>
    <project>myproject</project>
    <module>bla</module>
  </source>
  <timestamp>1180551778</timestamp>
  <body>
    <commit>
      <revision>%s</revision>
      <files></files>
      <author>Jelmer Vernooij &lt;jelmer@ganieda.lan.vernstok.nl&gt;</author>
      <log>bla</log>
    </commit>
  </body>
</message>

""" % (bzrlib.version_string, revid), self.run_bzr('cia-submit --dry-run')[0])

    def test_submit_dry_run_quiet(self):
        tree = self.make_branch_and_tree("bla")
        revid = tree.commit("bla", timestamp=1180551778, 
                committer="Jelmer Vernooij <jelmer@ganieda.lan.vernstok.nl>")

        tree.branch.get_config().set_user_option('cia_quiet', "True")

        os.chdir("bla")
        self.run_bzr('cia-project myproject')

        self.assertEquals("""
<message>
  <generator> 
    <name>bzr</name> 
    <version>%s</version> 
    <url>http://samba.org/~jelmer/bzr/cia_bzr.py</url>
  </generator>
  <source>
    <project>myproject</project>
    <module>bla</module>
  </source>
  <timestamp>1180551778</timestamp>
  <body>
    <commit>
      <revision>%s</revision>
      <files></files>
      <author>Jelmer Vernooij &lt;jelmer@ganieda.lan.vernstok.nl&gt;</author>
      <log>bla</log>
    </commit>
  </body>
</message>

""" % (bzrlib.version_string, revid), self.run_bzr('cia-submit --dry-run')[0])
