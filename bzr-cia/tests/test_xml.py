# Copyright (C) 2006-2007 Jelmer Vernooij <jelmer@samba.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""Tests for XML generator."""

import bzrlib
from bzrlib.plugins.cia.cia import generate_cia_xml
from bzrlib.tests import TestCaseWithTransport


class TestXML(TestCaseWithTransport):
    def test_simple(self):
        tree = self.make_branch_and_tree("bla")
        revid = tree.commit("bla", timestamp=1180551778, 
                committer="Jelmer Vernooij <jelmer@ganieda.lan.vernstok.nl>")

        xml = generate_cia_xml(tree.branch, revid, "myproject")

        self.assertEqualDiff("""
<message>
  <generator> 
    <name>bzr</name> 
    <version>%s</version> 
    <url>http://samba.org/~jelmer/bzr/cia_bzr.py</url>
  </generator>
  <source>
    <project>myproject</project>
    <module>bla</module>
  </source>
  <timestamp>1180551778</timestamp>
  <body>
    <commit>
      <revision>%s</revision>
      <files></files>
      <author>Jelmer Vernooij &lt;jelmer@ganieda.lan.vernstok.nl&gt;</author>
      <log>bla</log>
    </commit>
  </body>
</message>
""" % (bzrlib.version_string, revid), xml)

    def test_override_author(self):
        tree = self.make_branch_and_tree("bla")
        revid = tree.commit("bla", timestamp=1180551778)

        xml = generate_cia_xml(tree.branch, revid, "myproject", 
                author="somebody")

        self.assertEqualDiff("""
<message>
  <generator> 
    <name>bzr</name> 
    <version>%s</version> 
    <url>http://samba.org/~jelmer/bzr/cia_bzr.py</url>
  </generator>
  <source>
    <project>myproject</project>
    <module>bla</module>
  </source>
  <timestamp>1180551778</timestamp>
  <body>
    <commit>
      <revision>%s</revision>
      <files></files>
      <author>somebody</author>
      <log>bla</log>
    </commit>
  </body>
</message>
""" % (bzrlib.version_string, revid), xml)

    def test_override_revspec(self):
        tree = self.make_branch_and_tree("bla")
        revid = tree.commit("bla", timestamp=1180551778)

        xml = generate_cia_xml(tree.branch, revid, "myproject", 
                revname="42", author="somebody")

        self.assertEqualDiff("""
<message>
  <generator> 
    <name>bzr</name> 
    <version>%s</version> 
    <url>http://samba.org/~jelmer/bzr/cia_bzr.py</url>
  </generator>
  <source>
    <project>myproject</project>
    <module>bla</module>
  </source>
  <timestamp>1180551778</timestamp>
  <body>
    <commit>
      <revision>42</revision>
      <files></files>
      <author>somebody</author>
      <log>bla</log>
    </commit>
  </body>
</message>
""" % (bzrlib.version_string), xml)
