#!/usr/bin/env python

from distutils.core import setup

setup(
    name="bzr-cia",
    version="1.0.0",
    author="Jelmer Vernooij",
    author_email="jelmer@samba.org",
    url="http://bazaar-vcs.org/CIA",
    description="CIA submit plugin for Bazaar",
    license="GNU GPL v2 or later",
    package_dir={ "bzrlib.plugins.cia": "."},
    packages=["bzrlib.plugins.cia", 
              "bzrlib.plugins.cia.tests"],
)
