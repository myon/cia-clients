# Copyright (C) 2005-2011 Jelmer Vernooij <jelmer@samba.org>
 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Command implementations."""

from bzrlib.commands import Command
from bzrlib.option import Option


class cmd_cia_project(Command):
    """Print or set project to submit changes to on CIA.

    """
    takes_args = ['project?']
    takes_options = []

    def run(self, project=None):
        from bzrlib.branch import Branch
        br = Branch.open('.')

        config = br.get_config()
        if project:
            config.set_user_option('cia_project', project)
        else:
            self.outf.write("%s\n" % config.get_user_option('cia_project'))


class cmd_cia_submit(Command):
    """Submit a revision to CIA

    """
    takes_args = ['branch?']
    takes_options = ['revision', 
        Option('pending', 
            help="Submit all pending CIA submissions."),
        Option('dry-run', 
            help="Show what would be done, but don't actually do anything.")]

    def run(self, branch=None, revision=None, dry_run=False, pending=False):
        from bzrlib.plugins.cia.cia import (
            submit as cia_submit,
            submit_pending,
            )
        from bzrlib.branch import Branch
        from bzrlib.config import GlobalConfig
        from bzrlib.errors import BzrCommandError
        if pending and (branch is not None or revision is not None):
            raise BzrCommandError("--pending and specifying a branch are mutually exclusive")
        if pending:
            submit_pending(GlobalConfig())
        else:
            if branch is not None:
                branch = "."
            br = Branch.open(branch)

            if revision is None:
                revs = [br.last_revision()]
            elif len(revision) == 1:
                revs = [revision[0].in_history(br).rev_id]
            elif len(revision) == 2:
                if revision[0].spec is None:
                    from_revno = 1
                else:
                    from_revno = revision[0].in_history(br).revno

                if revision[1].spec is None:
                    to_revno = br.revno()
                else:
                    to_revno = revision[1].in_history(br).revno
                revs = br.revision_history()[from_revno:to_revno]
            else:
                raise BzrCommandError('bzr submit-cia --revision takes one or two arguments')

            for rev_id in revs:
                cia_submit(br, rev_id, br.revision_id_to_revno(rev_id), dry_run)
