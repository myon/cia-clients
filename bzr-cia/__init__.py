# Copyright (C) 2005-2009 Jelmer Vernooij <jelmer@samba.org>
 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Post-commit hook to submit the commit to CIA (http://cia.navi.cx/)

Requires bzr 0.15 or higher.

Copyright (C) 2005-2010 Jelmer Vernooij <jelmer@samba.org>
Published under the GNU GPLv2 or later

Installation:
Copy this directory to ~/.bazaar/plugins/cia/*

Configuration:
In ~/.bazaar/bazaar.conf, set 'cia_user' to your username on CIA.

Other options that can be set are 'cia_server' and 'cia_quiet', and
'cia_send_revno'

To enable CIA reporting for a particular branch, run:
$ bzr cia-project <name> 
inside that branch
"""

version_info = (1, 0, 0, 'dev', 0)


from bzrlib.branch import Branch
from bzrlib.commands import plugin_cmds


def branch_commit_hook(local, master, old_revno, old_revid, new_revno, new_revid):
    from bzrlib.plugins.cia.cia import submit as cia_submit
    if local is None:
        return cia_submit(master, new_revid, new_revno)
    else:
        return cia_submit(local, new_revid, new_revno)


plugin_cmds.register_lazy("cmd_cia_project", [], "bzrlib.plugins.cia.cmds")
plugin_cmds.register_lazy("cmd_cia_submit", [], "bzrlib.plugins.cia.cmds")
Branch.hooks.install_named_hook('post_commit', branch_commit_hook, 'CIA')

def test_suite():
    from unittest import TestSuite
    from bzrlib.plugins.cia import tests
    suite = TestSuite()
    suite.addTest(tests.test_suite())
    return suite
