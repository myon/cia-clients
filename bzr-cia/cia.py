# Copyright (C) 2005-2011 Jelmer Vernooij <jelmer@samba.org>
 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import bzrlib
from bzrlib.trace import note, warning


def generate_cia_xml(branch, revid, project, revname=None, author=None):
    from xml.sax import saxutils
    revision = branch.repository.get_revision(revid)
    delta = branch.repository.get_revision_delta(revid)

    if revname is None:
        revname = revid

    if author is None:
        authors = revision.get_apparent_authors()
    else:
        authors = [author]

    files = []
    [files.append(f) for (f,_,_) in delta.added]
    [files.append(f) for (f,_,_) in delta.removed]
    [files.append(f) for (_,f,_,_,_,_) in delta.renamed]
    [files.append(f) for (f,_,_,_,_) in delta.modified]

    authors_xml = "".join(["      <author>%s</author>\n" % saxutils.escape(author) for author in authors])

    return """
<message>
  <generator> 
    <name>bzr</name> 
    <version>%s</version> 
    <url>http://samba.org/~jelmer/bzr/cia_bzr.py</url>
  </generator>
  <source>
    <project>%s</project>
    <module>%s</module>
  </source>
  <timestamp>%d</timestamp>
  <body>
    <commit>
      <revision>%s</revision>
      <files>%s</files>
%s      <log>%s</log>
    </commit>
  </body>
</message>
""" % (bzrlib.version_string, project, branch.nick, revision.timestamp, revname,
        "\n".join(["<file>%s</file>" % saxutils.escape(f) for f in files]), 
        authors_xml, 
        saxutils.escape(revision.message))


def cia_cache_dir():
    from bzrlib.config import config_dir
    import os
    return os.path.join(config_dir(), "cia")


def store_failed_message(revid, message):
    import os
    cache_dir = cia_cache_dir()
    if not os.path.isdir(cache_dir):
        os.mkdir(cache_dir)
    cache_file = os.path.join(cache_dir, revid)
    f = open(cache_file, 'w')
    f.write(message)
    f.close()
    return cache_file


def cia_connect(config):
    import xmlrpclib
    server = config.get_user_option('cia_server')
    if server is None:
        server = "http://cia.vc"

    return xmlrpclib.ServerProxy(server)


class CIADeliverError(Exception):

    def __init__(self, message):
        self.message = message


def cia_deliver(server, msg):
    import socket, xmlrpclib
    try:
        server.hub.deliver(msg)
    except xmlrpclib.ProtocolError, e:
        raise CIADeliverError(e.errmsg)
    except socket.gaierror, (_, errmsg):
        raise CIADeliverError(errmsg)
    except socket.error, (_, errmsg):
        raise CIADeliverError(errmsg)


def submit(branch, revid, revno, dry_run=False):
    config = branch.get_config()

    project = config.get_user_option('cia_project')
    if project is None:
        return

    author = config.get_user_option('cia_user')
    quiet = config.get_user_option('cia_quiet')

    use_revno = config.get_user_option('cia_send_revno')
    if use_revno is not None and use_revno.lower()[:1] in ('t', '1'):
        revspec = revno
    else:
        revspec = revid

    msg = generate_cia_xml(branch, revid, project, revspec, author)

    if not quiet:
        note("Submitting revision to CIA.")
    if not dry_run:
        try:
            cia_deliver(cia_connect(config), msg)
        except CIADeliverError, error:
            warning("Unable to submit revision to CIA: %s" % error)
            store_failed_message(revid, msg)
            note("To retry submit later, run 'bzr cia-submit --pending'.")
    else:
        print msg


def submit_pending(config):
    import os
    server = cia_connect(config)
    cache_dir = cia_cache_dir()
    for revid in os.listdir(cache_dir):
        path = os.path.join(cache_dir, revid)
        msg = open(path, 'r').read()
        try:
            cia_deliver(server, msg)
        except CIADeliverError, error:
            warning("Submitting %s failed: %s" % (revid, error))
        else:
            os.remove(path)



